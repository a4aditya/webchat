/* eslint-disable linebreak-style */
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextareaAutosize from 'react-textarea-autosize';
import Send from 'assets/send_button';
import SendVoice from 'assets/send_voice';
import './style.scss';
import SendMedia from './SendMedia';

const Sender = ({ sendMessage, inputTextFieldHint, disabledInput, userInput }) => {
  const [inputValue, setInputValue] = useState('');
  const [transcript, setTranscript] = useState('');
  const [monologue, setMonologue] = useState('false');

  const formRef = useRef('');
  function handleChange(e) {
    setInputValue(e.target.value);
    setTranscript('');
  }

  function handleSubmit(e) {
    sendMessage(e);
    setInputValue('');
    setTranscript('');
  }

  function onEnterPress(e) {
    if (e.keyCode === 13 && e.shiftKey === false) {
      e.preventDefault();

      // by dispatching the event we trigger onSubmit
      // formRef.current.submit() would not trigger onSubmit
      formRef.current.dispatchEvent(new Event('submit', { cancelable: true }));
    }
  }

  function onVoiceClick(e) {
    const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    const recognition = new SpeechRecognition();
    recognition.lang = 'en-US';
    recognition.interimResults = true;
    // recognition.continuous = true
    recognition.start();
    setMonologue('true');
    this.props.monologue(monologue);

    recognition.addEventListener('result', (e) => {
      const last = e.results.length - 1;
      const text = e.results[last][0].transcript;

      setTranscript(text);
      console.log(`Confidence: ${e.results[0][0].confidence}`);
      console.log(text);
      setInputValue(text);
    });

    // eslint-disable-next-line no-shadow
    recognition.addEventListener('speechend', (e) => {
      setTranscript(transcript);
      setTranscript('');
      recognition.abort();
      setMonologue('false');
      this.props.monologue(monologue);
    });
  }

  return (
    userInput === 'hide' ? <div /> : (
      <><SendMedia constraints={{ audio: true }} />'     '<form ref={formRef} className="rw-sender" onSubmit={handleSubmit}>
        <TextareaAutosize type="text" minRows={1} value={transcript || inputValue} onKeyDown={onEnterPress} maxRows={3} onChange={handleChange} className="rw-new-message" name="message" placeholder={inputTextFieldHint} disabled={disabledInput || userInput === 'disable'} autoFocus autoComplete="off" />
        {!(inputValue && inputValue.length > 0) ? (
            <><button type="submit" className="rw-send" onClick={onVoiceClick}><SendVoice className="rw-send-icon" ready={!(inputValue && inputValue.length > 0)} alt="send" /></button></>
        ) : (
          <><button type="submit" className="rw-send" ><Send className="rw-send-icon" ready={!!(inputValue && inputValue.length > 0)} alt="send" /></button></>
        )
        }
      </form></>));
};

const mapStateToProps = state => ({
  userInput: state.metadata.get('userInput')
});

Sender.propTypes = {
  sendMessage: PropTypes.func,
  inputTextFieldHint: PropTypes.string,
  disabledInput: PropTypes.bool,
  userInput: PropTypes.string
};

export default connect(mapStateToProps)(Sender);
