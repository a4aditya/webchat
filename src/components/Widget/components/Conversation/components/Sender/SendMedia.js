import React from 'react'
import WithMediaRecorder from './WithMediaRecorder'

const SendMedia = ({ mediaRecorder }) => (
  <div>
    <h1>Media recorder</h1>
    <p>{(mediaRecorder.isRecording) && 'Recording...'}</p>
    <div>
      <div>{mediaRecorder.previewElement}</div>
      <div>{mediaRecorder.recordedElement}</div>
    </div>
    <button onClick={mediaRecorder.askPermissions}>start</button>
    {(mediaRecorder.isRecording)
      ? <button onClick={mediaRecorder.stopRecord}>stop</button>
      : <button onClick={mediaRecorder.record}>record</button>
    }
  </div>
)

export default WithMediaRecorder(SendMedia)