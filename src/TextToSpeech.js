import React, { Component } from 'react';

class TextToSpeech extends Component {
  constructor(props) {
    super(props);

    if ('speechSynthesis' in window) {
      this.speech = this.createSpeech();
    } else {
      console.warn('The current browser does not support the speechSynthesis API.');
    }

    this.state = {
      started: false,
      playing: false
    };
  }

  createSpeech = () => {
    const defaults = {
      text: '',
      volume: 1,
      rate: 1,
      pitch: 1,
      lang: 'en-US'
    };

    const speech = new SpeechSynthesisUtterance();

    Object.assign(speech, defaults, this.props);

    return speech;
  }

  speak = () => {
    window.speechSynthesis.speak(this.speech);
    this.setState({ started: true, playing: true });
  }


  shouldComponentUpdate() {
    return false;
  }

  componentDidMount() {
    const events = [
      { name: 'start', action: this.props.onStart },
      { name: 'error', action: this.props.onError }
    ];

    events.forEach((e) => {
      this.speech.addEventListener(e.name, e.action);
    });

    this.speech.addEventListener('end', () => {
      this.setState({ started: false });
      // this.props.onEnd()
    });

    if (this.props.play) {
      this.speak();
    }
  }

  componentWillUnmount() {
    this.cancel();
  }

  render() {
    return null;
  }
}

export default TextToSpeech;
