/* eslint-disable linebreak-style */
import PropTypes from 'prop-types';
import React, { useContext } from 'react';
import ThemeContext from '../src/components/Widget/ThemeContext';

function SendVoice({ ready }) {
  const { mainColor } = useContext(ThemeContext);

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="25"
      height="25"
      version="1.1"
      xmlSpace="preserve"
    >
      <path
        className={ready ? 'rw-send-icon-ready' : 'rw-send-icon'}
        style={{ fill: ready && mainColor }}
        d="M15.088 8.609c.504 0 .912.47.912 1.052 0 4.734-3.105 8.643-7.088 9.168v2.119C8.912 21.528 8.504 22 8 22s-.911-.471-.911-1.052v-2.119C3.105 18.305 0 14.395 0 9.662 0 9.08.408 8.61.912 8.61c.503 0 .911.47.911 1.052 0 3.93 2.771 7.128 6.177 7.128 3.406 0 6.177-3.198 6.177-7.128 0-.582.408-1.052.911-1.052zM8.164 0c1.878 0 3.46 1.456 3.944 3.444.066.273-.11.543-.354.543h-1.066c-.322 0-.583.301-.583.672 0 .374.261.675.583.675h.983c.325 0 .59.304.59.68 0 .375-.264.68-.59.68h-.983c-.322 0-.583.301-.583.674 0 .371.261.673.583.673h.983c.325 0 .59.305.59.681 0 .375-.264.679-.59.679h-.983c-.322 0-.583.302-.583.675 0 .37.261.672.583.672h1.014c.25 0 .428.282.35.556-.54 1.879-2.076 3.234-3.888 3.234h-.329c-2.262 0-4.095-2.116-4.095-4.726V4.727C3.74 2.117 5.573 0 7.835 0h.33z"
      />
    </svg>
  );
}


SendVoice.propTypes = {
  ready: PropTypes.bool
};

export default SendVoice;

